# frozen_string_literal: true

require 'gitlab'
require 'delegate'

require_relative 'items/topic'
require_relative 'cacher'
require_relative 'log'

module Discoto
  class IssuableWrapper < SimpleDelegator
    attr_accessor :type
    def initialize(type, item)
      super(item)
      @type = type
    end

    def ref
      if @type == 'issue'
        iid
      elsif @type == 'epic'
        id
      elsif @type == 'merge_request'
        iid
      end
    end
  end

  class DiscussionWrapper < SimpleDelegator
    attr_accessor :issuable
    def initialize(issuable, item)
      super(item)
      @issuable = issuable
    end
  end

  ContainerInfo = Struct.new(:type, :id)
  NoteableInfo = Struct.new(:type, :id, :iid)

  class Loader
    include Cacher
    include Logging

    def initialize(client, config)
      cache_init

      @client = client
      @user = @client.user
      @config = config
    end

    def load_container(cinfo)
      if cinfo.type == 'group'
        load_group(cinfo.id)
      elsif cinfo.type == 'project'
        load_project(cinfo.id)
      end
    end

    def load_project(project_path)
      project = nil
      if project_path.is_a?(Integer)
        project = cache(:project_ids, project_path) do
          @client.project(project_path)
        end
        project_path = project.path_with_namespace
      end

      cache(:projects, project_path) do
        project || @client.project(project_path)
      end
    end

    def load_group(group_path)
      group = nil
      if group_path.is_a?(Integer)
        group = cache(:group_ids, group_path) do
          @client.group(group_path)
        end
        group_path = group.path
      end

      cache(:groups, group_path) do
        group || @client.group(group_path)
      end
    end

    def load_topic(issuable_type, issuable_iid, container_type, container_id)
      container_id = ensure_container_id(container_type, container_id)

      cache(:topics, "#{container_type}-#{container_id}-#{issuable_type}-#{issuable_iid}") do
        if issuable_type == 'issue'
          issuable = load_issue(issuable_iid, container_id)
        elsif issuable_type == 'epic'
          issuable = load_epic(issuable_iid, container_id)
        elsif issuable_type == 'merge_request'
          issuable = load_merge_request(issuable_iid, container_id)
        end
        Items::Topic.new(self, issuable)
      end
    end

    # load all topics from the given project and group paths
    def load_topics
      res = []

      @config[:projects].each do |project_path|
        project = load_project(project_path)
        next if project.nil?

        res += load_issues(project.id).map do |issue|
          cache(:topics, "project-#{project.id}-issue-#{issue.iid}") do
            Items::Topic.new(self, issue)
          end
        end

        res += load_merge_requests(project.id).map do |mr|
          cache(:topics, "project-#{project.id}-merge_request-#{mr.iid}") do
            Items::Topic.new(self, mr)
          end
        end
      end

      @config[:groups].each do |group_path|
        group = load_group(group_path)
        next if group.nil?

        res += load_epics(group.id).each.map do |epic|
          cache(:topics, "group-#{group.id}-epic-#{epic.iid}") do
            Items::Topic.new(self, epic)
          end
        end
      end

      res
    end

    def load_epic(epic_iid, group_id)
      group_id = ensure_group_id(group_id)
      item_key = cache_issuable_key(epic_iid, group_id)
      cache(:epics, item_key) do
        IssuableWrapper.new('epic', @client.epic(group_id, epic_iid))
      end
    end

    def load_merge_request(mr_iid, project_id)
      project_id = ensure_project_id(project_id)
      item_key = cache_issuable_key(mr_iid, project_id)
      cache(:merge_requests, item_key) do
        IssuableWrapper.new('merge_request', @client.merge_request(project_id, mr_iid))
      end
    end

    def load_issue(issue_iid, project_id)
      project_id = ensure_project_id(project_id)
      item_key = cache_issuable_key(issue_iid, project_id)
      cache(:issues, item_key) do
        IssuableWrapper.new('issue', @client.issue(project_id, issue_iid))
      end
    end

    def create_issue(project_id, title, labels: nil, description: nil)
      project_id = ensure_project_id(project_id)

      opts = {}
      opts[:labels] = labels unless labels.nil?
      opts[:description] = description unless description.nil?
      res = @client.create_issue(project_id, title, opts)

      cache_add_issuable(IssuableWrapper.new('issue', res))
    end

    def issuable_filter
      res = {
        state: 'opened',
        labels: @config[:labels]
      }

      author = @config[:author].to_s
      res[:author_id] = load_user(author).id unless author.empty?

      assignee = @config[:assignee].to_s
      res[:assignee_id] = load_user(assignee).id unless assignee.empty?

      res
    end

    def load_issues(project_id)
      project_id = ensure_project_id(project_id)
      cache(:initial_issues, project_id) do
        open_issues = @client.issues(project_id, issuable_filter)
        open_issues.auto_paginate.map do |issue|
          cache_add_issuable(IssuableWrapper.new('issue', issue))
        end
      end
    end

    def load_merge_requests(project_id)
      project_id = ensure_project_id(project_id)
      cache(:initial_merge_requests, project_id) do
        open_mrs = @client.merge_requests(project_id, issuable_filter)
        open_mrs.auto_paginate.map do |mr|
          cache_add_issuable(IssuableWrapper.new('merge_request', mr))
        end
      end
    end

    def load_epics(group_id)
      cache(:initial_epics, group_id) do
        open_epics = @client.epics(group_id, issuable_filter)
        open_epics.auto_paginate.map do |epic|
          cache_add_issuable(IssuableWrapper.new('epic', epic))
        end
      rescue Gitlab::Error::Forbidden
        []
      end
    end

    def load_user(username)
      username = username[1..] if username.start_with?('@')
      cache(:users, username) do
        @client.users({ username: username })[0]
      end
    end

    def set_issuable_description(issuable, description)
      cinfo = container_info(issuable)
      if issuable.type == 'issue'
        res = @client.edit_issue(cinfo.id, issuable.ref, { description: description })
      elsif issuable.type == 'epic'
        # yes, iid here and not ref. This is frustratingly inconsistent
        res = @client.edit_epic(cinfo.id, issuable.iid, { description: description })
      elsif issuable.type == 'merge_request'
        res = @client.update_merge_request(cinfo.id, issuable.ref, { description: description })
      end
      issuable.__setobj__(res)
      cache_add_issuable(issuable)
    end

    def load_discussions(issuable)
      type = issuable.type
      cinfo = container_info(issuable)
      res = cache(:discussions, "#{cinfo.type}-#{cinfo.id}-#{type}-#{issuable.iid}") do
        discussions = @client.get("/#{cinfo.type}s/#{cinfo.id}/#{type}s/#{issuable.ref}/discussions")
        res = discussions.auto_paginate.filter do |disc|
          disc.notes[0]['system'] == false
        end
        res.map do |disc|
          DiscussionWrapper.new(issuable, _ensure_noteable_ids(issuable, disc))
        end
      end

      res
    end

    def create_discussion(issuable, body)
      cinfo = container_info(issuable)
      type = issuable.type
      path = "/#{cinfo.type}s/#{cinfo.id}/#{type}s/#{issuable.ref}/discussions"
      args = URI.encode_www_form('body' => body)

      disc = @client.post("#{path}?#{args}")
      disc = _ensure_noteable_ids(issuable, disc)
      DiscussionWrapper.new(issuable, disc)
    end

    def add_note_to_discussion(discussion, body)
      cinfo = container_info(discussion.issuable)
      ninfo = noteable_info(discussion.notes[0])

      noteable_ref = cinfo.type == 'group' ? ninfo.id : ninfo.iid

      path = "/#{cinfo.type}s/#{cinfo.id}/#{ninfo.type}s/#{noteable_ref}/discussions/#{discussion.id}/notes"
      args = URI.encode_www_form('body' => body)
      @client.post("#{path}?#{args}")
    end

    def _ensure_noteable_ids(issuable, disc)
      # discussion notes in epics don't always set the noteable_iid
      new_notes = disc.notes.map do |note|
        note_hash = note.to_hash
        note_hash['ensured'] = true
        note_hash['noteable_id'] = issuable.id
        note_hash['noteable_iid'] = issuable.iid
        Gitlab::ObjectifiedHash.new(note_hash)
      end

      disc_hash = disc.to_hash
      disc_hash['ensured'] = true
      disc_hash['notes'] = new_notes
      Gitlab::ObjectifiedHash.new(disc_hash)
    end

    def note_has_emoji?(issuable, note, emoji_name)
      cinfo = container_info(issuable)
      ninfo = noteable_info(note)

      emojis = @client.note_award_emojis(
        cinfo.id,
        ninfo.iid || ninfo.id,
        ninfo.type,
        note['id']
      )
      emojis.each do |emoji|
        return true if emoji.user.id == @user.id && emoji.name == emoji_name
      end
      false
    end

    def note_add_emoji(issuable, note, emoji_name)
      cinfo = container_info(issuable)
      ninfo = noteable_info(note)
      @client.create_note_award_emoji(
        cinfo.id,
        ninfo.iid,
        ninfo.type,
        note['id'],
        emoji_name
      )
    end

    def url_for_note(issuable, note)
      "#{issuable_url_for_note(issuable, note)}#note_#{note['id']}"
    end

    def issuable_url_for_note(issuable, note)
      cinfo = container_info(issuable)
      container = load_container(cinfo)
      ninfo = noteable_info(note)
      ninfo.iid ||= issuable.iid

      container_url = container.web_url
      "#{container_url}/-/#{ninfo.type}s/#{ninfo.iid}"
    end

    def container_info(issuable)
      if !issuable['group_id'].nil?
        ContainerInfo.new('group', issuable.group_id)
      elsif !issuable['project_id'].nil?
        ContainerInfo.new('project', issuable.project_id)
      end
    end

    def noteable_info(note)
      type = note['noteable_type'].gsub(/([a-z])([A-Z])/, '\1_\2').downcase
      iid = note['noteable_iid']
      id = note['noteable_id']
      NoteableInfo.new(type, id, iid)
    end

    def ensure_container_id(ctype, cid)
      if ctype == 'project'
        ensure_project_id(cid)
      elsif ctype == 'group'
        ensure_group_id(cid)
      end
    end

    def ensure_project_id(project_id)
      return project_id unless project_id.is_a?(String)

      project = load_project(project_id)
      project.id
    end

    def ensure_group_id(group_id)
      return group_id unless group_id.is_a?(String)

      group = load_group(group_id)
      group.id
    end

    def cache_issuable_key(issuable_iid, container_id)
      "#{container_id}-#{issuable_iid}"
    end

    def cache_add_issuable(issuable)
      cache_key = (issuable.type + 's').to_sym

      if %i[merge_requests issues].include?(cache_key)
        item_key = cache_issuable_key(issuable.iid, issuable.project_id)
      elsif cache_key == :epics
        item_key = cache_issuable_key(issuable.iid, issuable.group_id)
      else
        raise StandardError, "Unknown issuable type: #{cache_key}"
      end
      cache_add(cache_key, item_key, issuable)
    end
  end
end
